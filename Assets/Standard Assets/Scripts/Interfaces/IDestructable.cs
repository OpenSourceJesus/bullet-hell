﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BulletHell
{
	public interface IDestructable
	{
		int Hp { get; set; }
		int MaxHp { get; set; }
		
		void TakeDamage (int amount);
		void Death ();
	}
}