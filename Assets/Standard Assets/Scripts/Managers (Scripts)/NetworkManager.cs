using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkRift.Client.Unity;
using DarkRift.Server.Unity;
using DarkRift.Client;
using DarkRift;
using System;
using TMPro;
using Extensions;
using Random = UnityEngine.Random;

namespace BulletHell
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public UnityClient client;
		public XmlUnityServer server;
		public Player playerPrefab;
		public SpawnPoint[] spawnPoints = new SpawnPoint[0];
		public static Dictionary<ushort, Player> playersDict = new Dictionary<ushort, Player>();
		public static Dictionary<uint, SyncTransform> syncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static bool isConnected;

		void Start ()
		{
			if (client.enabled)
			{
				syncTransformsDict.Clear();
				SyncTransform[] npcSyncTransforms = FindObjectsOfType<SyncTransform>();
				for (int i = 0; i < npcSyncTransforms.Length; i ++)
				{
					SyncTransform npcSyncTrs = npcSyncTransforms[i];
					syncTransformsDict.Add(npcSyncTrs.id, npcSyncTrs);
				}
			}
#if UNITY_EDITOR
			if (client.enabled)
				Connect ();
#endif
		}
		
		public void Connect ()
		{
			print("Connecting to " + client.Host + " on port " + client.Port);
			client.Disconnected -= OnDisconnected;
			client.Disconnected += OnDisconnected;
			client.MessageReceived -= OnMessageReceived;
			client.MessageReceived += OnMessageReceived;
			client.ConnectInBackground(client.Host, client.Port, false, OnConnectDone);
		}

		void OnConnectDone (Exception e)
		{
			if (e != null)
			{
				print(e.Message + "\n" + e.StackTrace);
				Connect ();
			}
			else
			{
				isConnected = true;
				SpawnPlayer ();
			}
		}

		void OnDisconnected (object sender, DisconnectedEventArgs eventArgs)
		{
			DRMManager.instance.EndGame ();
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				if (message.Tag == MessageTags.SPAWN_PLAYER)
					OnSpawnPlayer (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_MOVED)
					OnSyncTransformMoved (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_ROTATED)
					OnSyncTransformRotated (sender, eventArgs);
				else if (message.Tag == MessageTags.PLAYER_LEFT)
					OnPlayerLeft (sender, eventArgs);
				// else if (message.Tag == MessageTags.PLAYER_STARTED_ACTION)
				// 	OnPlayerStartedAction (sender, eventArgs);
			}
		}

		void OnSpawnPlayer (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						ushort playerId = reader.ReadUInt16();
						byte playerPrefabIndex = reader.ReadByte();
						Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						Quaternion rotation = Quaternion.Euler(Vector3.up * reader.ReadSingle());
						if (!playersDict.ContainsKey(playerId))
						{
							Player player = SpawnPlayer(position, rotation, playerId);
							playersDict.Add(playerId, player);
							syncTransformsDict.Add(playerId, player.syncTrs);
							if (playerId == client.ID)
								player.enabled = true;
						}
					}
				}
			}
		}

		void OnSyncTransformMoved (object sender, MessageReceivedEventArgs eventArgs)
		{
			print("OnSyncTransformMoved");
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						SyncTransform syncTrs;
						if (syncTransformsDict.TryGetValue(id, out syncTrs))
						{
							print("Moved");
							syncTrs.trs.position = position;
						}
					}
				}
			}
		}

		void OnSyncTransformRotated (object sender, MessageReceivedEventArgs eventArgs)
		{
			print("OnSyncTransformRotated");
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						float yRotation = reader.ReadSingle();
						SyncTransform syncTrs;
						if (syncTransformsDict.TryGetValue(id, out syncTrs))
						{
							print("Rotated");
							syncTrs.trs.eulerAngles = syncTrs.trs.eulerAngles.SetY(yRotation);
						}
					}
				}
			}
		}

		void OnPlayerLeft (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						ushort id = reader.ReadUInt16();
						Player player;
						if (playersDict.TryGetValue(id, out player))
						{
							Destroy(player.gameObject);
							playersDict.Remove(id);
							syncTransformsDict.Remove(id);
						}
					}
				}
			}
		}

		// void OnPlayerStartedAction (object sender, MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			while (reader.Position < reader.Length)
		// 			{
		// 				ushort playerId = reader.ReadUInt16();
		// 				byte actionId = reader.ReadByte();
		// 				double timeStarted = reader.ReadDouble();
		// 				Player player = playersDict[playerId];
		// 				Player.Action action = player.actions[actionId];
		// 				if (action.animatorStateLayerId != -1)
		// 				{
		// 					float normalizedTime = (float) (GameManager.GetTimeInSeconds() - timeStarted) / action.animatorStateDuration;
		// 					player.animator.Play(action.animatorStateName, action.animatorStateLayerId, normalizedTime);
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		public Player SpawnPlayer ()
		{
			if (this != instance)
				return instance.SpawnPlayer();
			byte spawnPointIndex = (byte) Random.Range(0, spawnPoints.Length);
			SpawnPoint spawnPoint = spawnPoints[spawnPointIndex];
			Player player = SpawnPlayer(spawnPoint.trs.position, spawnPoint.trs.rotation, client.ID);
			player.enabled = true;
			// currentPlayerText.text = "You are: " + client.ID;
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(spawnPointIndex);
				using (Message newMessage = Message.Create(MessageTags.SPAWN_PLAYER, writer))
					client.SendMessage(newMessage, SendMode.Reliable);
			}
			return player;
		}

		public Player SpawnPlayer (Vector3 position, Quaternion rotation, ushort id)
		{
			if (this != instance)
				return instance.SpawnPlayer(position, rotation, id);
			Player output = ObjectPool.instance.SpawnComponent<Player>(playerPrefab.prefabIndex, position, rotation);
			output.ID = id;
			return output;
		}

		[Serializable]
		public class SpawnPoint// : IHasDataFile
		{
			// [fsIgnore]
			public Transform trs;
			// [fsProperty]
			// public Vector3 Position
			// {
			// 	get
			// 	{
			// 		return trs.position;
			// 	}
			// }
			// [fsProperty]
			// public float YEulerAngles
			// {
			// 	get
			// 	{
			// 		return trs.eulerAngles.y;
			// 	}
			// }
			// public uint id;
			// public uint ID
			// {
			// 	get
			// 	{
			// 		return id;
			// 	}
			// 	set
			// 	{
			// 		id = value;
			// 	}
			// }
			// public string DataFilePath
			// {
			// 	get
			// 	{
			// 		return GameManager.dataPath + "/SpawnPoint Data.txt";
			// 	}
			// }
		}

		public struct MessageTags
		{
			public const ushort SPAWN_PLAYER = 0;
			public const ushort SYNC_TRANSFORM_MOVED = 1;
			public const ushort SYNC_TRANSFORM_ROTATED = 2;
			public const ushort PLAYER_DIED = 3;
			public const ushort PLAYER_LEFT = 4;
			public const ushort PLAYER_STARTED_ACTION = 5;
		}
	}
}
