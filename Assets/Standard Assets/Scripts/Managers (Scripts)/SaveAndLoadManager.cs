﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using Extensions;
using FullSerializer;
using System;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
{
	public static fsSerializer serializer = new fsSerializer();
	// [HideInInspector]
	public List<SaveAndLoadObject> saveAndLoadObjects = new List<SaveAndLoadObject>();
	public static SaveEntry[] saveEntries;
	public static int MostRecentlyLoadedSaveEntryIndex
	{
		get
		{
			return PlayerPrefs.GetInt("Most recently loaded save entry index", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Most recently loaded save entry index", value);
		}
	}
	public static int LastSaveEntryIndex
	{
		get
		{
			return PlayerPrefs.GetInt("Last save entry index", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Last save entry index", value);
		}
	}
	// public static Dictionary<string, SaveAndLoadObject> saveAndLoadObjectTypeDict = new Dictionary<string, SaveAndLoadObject>();
	public TemporaryActiveText displayOnSave;
	
#if UNITY_EDITOR
	public void OnEnable ()
	{
		if (Application.isPlaying)
		{
			if (displayOnSave.go != null)
				displayOnSave.go.SetActive(false);
			return;
		}
		// saveAndLoadObjects.Clear();
		// saveAndLoadObjects.AddRange(FindObjectsOfType<SaveAndLoadObject>());
		for (int i = 0; i < saveAndLoadObjects.Count; i ++)
		{
			SaveAndLoadObject saveAndLoadObject = saveAndLoadObjects[i];
			if (saveAndLoadObject.uniqueId == 0)
				saveAndLoadObject.uniqueId = Random.Range(int.MinValue, int.MaxValue);
		}
	}
#endif
	
	public void Start ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		// saveAndLoadObjectTypeDict.Clear();
		List<SaveEntry> saveEntries = new List<SaveEntry>();
		for (int i = 0; i < saveAndLoadObjects.Count; i ++)
		{
			SaveAndLoadObject saveAndLoadObject = saveAndLoadObjects[i];
			saveAndLoadObject.Init ();
			saveEntries.AddRange(saveAndLoadObject.saveEntries);
		}
		SaveAndLoadManager.saveEntries = saveEntries.ToArray();
		if (MostRecentlyLoadedSaveEntryIndex != 0)
			LoadMostRecent ();
	}
	
	public void Save ()
	{
		if (Instance != this)
		{
			instance.Save ();
			return;
		}
		MostRecentlyLoadedSaveEntryIndex ++;
		if (MostRecentlyLoadedSaveEntryIndex > LastSaveEntryIndex)
			LastSaveEntryIndex ++;
		for (int i = 0; i < saveEntries.Length; i ++)
			saveEntries[i].Save ();
		if (displayOnSave.go != null)
			StartCoroutine(displayOnSave.DoRoutine ());
	}
	
	public void Load (int savedGameIndex)
	{
		if (Instance != this)
		{
			instance.Load (savedGameIndex);
			return;
		}
		MostRecentlyLoadedSaveEntryIndex = savedGameIndex;
		StartCoroutine(LoadRoutine ());
	}

	public IEnumerator LoadRoutine ()
	{
		yield return new WaitForEndOfFrame();
		for (int i = 0; i < saveEntries.Length; i ++)
			saveEntries[i].Load ();
	}
	
	public void LoadMostRecent ()
	{
		Load (MostRecentlyLoadedSaveEntryIndex);
	}

	public static string Serialize (object value, Type type)
	{
		fsData data;
		serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();
		return fsJsonPrinter.CompressedJson(data);
	}
	
	public static object Deserialize (string serializedState, Type type)
	{
		fsData data = fsJsonParser.Parse(serializedState);
		object deserialized = null;
		serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();
		return deserialized;
	}
	
	public class SaveEntry
	{
		public SaveAndLoadObject saveableAndLoadObject;
		public ISaveableAndLoadable saveableAndLoadable;
		public PropertyInfo[] properties;
		public FieldInfo[] fields;
		public const string VALUE_SEPERATOR = "Ⅰ";
		
		public SaveEntry ()
		{
		}
		
		public void Save ()
		{
			for (int i = 0; i < properties.Length; i ++)
			{
				PropertyInfo property = properties[i];
				PlayerPrefs.SetString(MostRecentlyLoadedSaveEntryIndex + VALUE_SEPERATOR + saveableAndLoadObject.uniqueId + VALUE_SEPERATOR + property.Name, Serialize(property.GetValue(saveableAndLoadable, null), property.PropertyType));
			}
			for (int i = 0; i < fields.Length; i ++)
			{
				FieldInfo field = fields[i];
				PlayerPrefs.SetString(MostRecentlyLoadedSaveEntryIndex + VALUE_SEPERATOR + saveableAndLoadObject.uniqueId + VALUE_SEPERATOR + field.Name, Serialize(field.GetValue(saveableAndLoadable), field.FieldType));
			}
		}
		
		public void Load ()
		{
			object value;
			for (int i = 0; i < properties.Length; i ++)
			{
				PropertyInfo property = properties[i];
				value = Deserialize(PlayerPrefs.GetString(MostRecentlyLoadedSaveEntryIndex + VALUE_SEPERATOR + saveableAndLoadObject.uniqueId + VALUE_SEPERATOR + property.Name, Serialize(property.GetValue(saveableAndLoadable, null), property.PropertyType)), property.PropertyType);
				property.SetValue(saveableAndLoadable, value, null);
			}
			for (int i = 0; i < fields.Length; i ++)
			{
				FieldInfo field = fields[i];
				value = Deserialize(PlayerPrefs.GetString(MostRecentlyLoadedSaveEntryIndex + VALUE_SEPERATOR + saveableAndLoadObject.uniqueId + VALUE_SEPERATOR + field.Name, Serialize(field.GetValue(saveableAndLoadable), field.FieldType)), field.FieldType);
				field.SetValue(saveableAndLoadable, value);
			}
		}
	}
}
