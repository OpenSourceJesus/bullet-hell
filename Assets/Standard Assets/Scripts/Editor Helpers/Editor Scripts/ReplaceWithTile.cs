#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace BulletHell
{
	public class ReplaceWithTile : EditorScript
	{
		public Transform trs;
		public Tile replaceWithTilePrefab;

		public override void Do ()
		{
			if (this == null)
				return;
			if (trs == null)
				trs = GetComponent<Transform>();
			if (replaceWithTilePrefab != null)
			{
				Tile tile = (Tile) PrefabUtility.InstantiatePrefab(replaceWithTilePrefab);
				tile.trs.position = trs.position;
				tile.trs.rotation = trs.rotation;
				tile.trs.SetParent(trs.parent);
				tile.trs.localScale = trs.localScale;
				GameManager.DestroyOnNextEditorUpdate (gameObject);
			}
		}
	}
}
#else
namespace BulletHell
{
	public class ReplaceWithTile : EditorScript
	{
	}
}
#endif