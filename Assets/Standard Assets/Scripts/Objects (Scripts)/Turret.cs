using UnityEngine;
using Extensions;

namespace BulletHell
{
	public class Turret : UpdateWhileEnabled
	{
		public Timer reloadTimer;
		public Bullet bulletPrefab;
		public float bulletWidth;
		public LineRenderer line;
		public Color lockedOnColor;
		public Color searchingColor;
		public Transform trs;
		public Laser laser;
		public Collider2D collider;
		public AudioClip shootAudioClip;
		public Animator animator;
		public AnimationClip animationClip;
		Vector3? shootDirection;

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			shootDirection = null;
			trs.up = Player.instance.trs.position - trs.position;
			laser.DoUpdate ();
			if (laser.hit.collider != null && laser.hit.transform == Player.instance.trs)
			{
				shootDirection = trs.up;
				line.SetPosition(1, Player.instance.trs.position);
			}
			if (shootDirection != null)
			{
				line.startColor = lockedOnColor;
				line.endColor = lockedOnColor;
				if (reloadTimer.timeRemaining <= 0)
				{
					reloadTimer.Reset ();
					reloadTimer.Start ();
					Bullet bullet = ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, trs.position, Quaternion.LookRotation((Vector3) shootDirection));
					bullet.shooterCollider = collider;
					Physics2D.IgnoreCollision(collider, bullet.collider, true);
					AudioManager.instance.MakeSoundEffect (shootAudioClip, trs.position);
				}
				else if (reloadTimer.timeRemaining <= animationClip.length)
					animator.Play("Shoot");
			}
			else
			{
				trs.up = Player.instance.trs.position - trs.position;
				line.startColor = searchingColor;
				line.endColor = searchingColor;
			}
		}
	}
}