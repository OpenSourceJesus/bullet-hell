using UnityEngine;
using Extensions;

namespace BulletHell
{
	public class Star : SingletonMonoBehaviour<Star>
	{
		public Transform trs;
		public bool isCollected;
		public AudioClip onTriggerEnterAudioClip;

		void OnTriggerEnter (Collider other)
		{
			isCollected = true;
			gameObject.SetActive(false);
			AudioManager.instance.MakeSoundEffect (onTriggerEnterAudioClip, trs.position);
		}
	}
}