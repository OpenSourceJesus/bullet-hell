using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BulletHell
{
	public class BouncyTile : Tile, ICollisionEnterHandler2D
	{
		public Collider2D collider;
		public Collider2D Collider
		{
			get
			{
				return collider;
			}
		}
		public float forceAmount;
		public float bounceCooldown;
		public AnimationCurve positionCurve;
		List<Rigidbody2D> bouncingRigids = new List<Rigidbody2D>();

		public void OnCollisionEnter2D (Collision2D coll)
		{
			Rigidbody2D rigid = coll.gameObject.GetComponentInParent<Rigidbody2D>();
			if (rigid == null || bouncingRigids.Contains(rigid))
				return;
			bouncingRigids.Add(rigid);
			Vector2 forceDirection = new Vector2();
			for (int i = 0; i < coll.contactCount; i ++)
			{
				ContactPoint2D contactPoint = coll.GetContact(i);
				forceDirection -= contactPoint.normal;
			}
			StartCoroutine(BounceRoutine (rigid, forceDirection));
		}

		IEnumerator BounceRoutine (Rigidbody2D rigid, Vector2 forceDirection)
		{
			Vector2 position = trs.localPosition;
			rigid.AddForce(forceDirection.normalized * forceAmount, ForceMode2D.Impulse);
			float time = Time.time;
			while (true)
			{
				trs.localPosition = position + forceDirection.normalized * positionCurve.Evaluate((Time.time - time) / bounceCooldown);
				if (Time.time - time >= bounceCooldown)
					break;
				yield return new WaitForEndOfFrame();
			}
			bouncingRigids.Remove(rigid);
		}

		void OnCollisionStay2D (Collision2D coll)
		{
			OnCollisionEnter2D (coll);
		}
	}
}