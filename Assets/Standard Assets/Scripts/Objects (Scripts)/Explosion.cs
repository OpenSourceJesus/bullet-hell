using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BulletHell
{
	public class Explosion : Hazard
	{
		bool dead;

		void OnTriggerEnter (Collider other)
		{
			if (dead)
				return;
			dead = true;
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
		}

		public void DestroyMe ()
		{
			Destroy(gameObject);
		}
	}
}