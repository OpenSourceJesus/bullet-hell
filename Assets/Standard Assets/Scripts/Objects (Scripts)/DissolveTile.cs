using UnityEngine;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace BulletHell
{
	[ExecuteInEditMode]
	public class DissolveTile : Tile, ICollisionEnterHandler2D
	{
		public Collider2D collider;
		public Collider2D Collider
		{
			get
			{
				return collider;
			}
		}
		public float dissolveDuration;
		public AudioSource audioSource;
#if UNITY_EDITOR
		public bool autoSetBorder = true;
		public FollowWaypoints.WaypointPath borderGraphicsStyle;
#endif
		bool isDissolving;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (!autoSetBorder)
				return;
			autoSetBorder = false;
			LineRenderer[] lineRenderers = GetComponentsInChildren<LineRenderer>();
			for (int i = 0; i < lineRenderers.Length; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				LineRendererUtilities.RemoveLineRendererAndGameObjectIfEmpty (lineRenderer, true);
			}
			lineRenderers = new LineRenderer[12];
			for (int i = 0; i < 12; i ++)
				lineRenderers[i] = LineRendererUtilities.AddLineRendererToGameObjectOrMakeNew(gameObject, borderGraphicsStyle);
			LineRendererUtilities.SetLineRenderersToBoundsSides (meshRenderer.bounds, lineRenderers);
			for (int i = 0; i < 12; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				lineRenderer.SetUseWorldSpace (false);
			}
		}
#endif
		
		public override void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDestroy ();
		}

		public void OnCollisionEnter2D (Collision2D coll)
		{
			if (isDissolving)
				return;
			isDissolving = true;
			audioSource.Play();
			StartCoroutine(DissolveRoutine ());
		}

		IEnumerator DissolveRoutine ()
		{
			meshRenderer.material.ChangeMaterialTransparency (true);
			do
			{
				yield return new WaitForEndOfFrame();
				meshRenderer.material.color = meshRenderer.material.color.AddAlpha(-Time.deltaTime / dissolveDuration);
				if (meshRenderer.material.color.a <= 0)
				{
					meshRenderer.material.color = meshRenderer.material.color.SetAlpha(0);
					break;
				}
			} while (true);
			Destroy(gameObject);
		}
	}
}