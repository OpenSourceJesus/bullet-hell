using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace BulletHell
{
	public class StickyTile : Tile, ICollisionEnterHandler2D, IUpdatable
	{
		public Collider2D collider;
		public Collider2D Collider
		{
			get
			{
				return collider;
			}
		}
		List<StuckObject> stuckObjects = new List<StuckObject>();

		public void OnCollisionEnter2D (Collision2D coll)
		{
			Rigidbody2D rigid = coll.gameObject.GetComponentInParent<Rigidbody2D>();
			StuckObject stuckObject;
			for (int i = 0; i < stuckObjects.Count; i ++)
			{
				stuckObject = stuckObjects[i];
				if (stuckObject.rigid == rigid)
					return;
			}
			rigid.gravityScale = 0;
			rigid.velocity = Vector2.zero;
			rigid.angularVelocity = 0;
			stuckObject = new StuckObject(rigid, rigid.GetComponent<Transform>());
			stuckObjects.Add(stuckObject);
			stuckObject.trs.SetParent(trs);
		}

		void OnCollisionStay2D (Collision2D coll)
		{
			OnCollisionEnter2D (coll);
		}

		public override void OnEnable ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			for (int i = 0; i < stuckObjects.Count; i ++)
			{
				StuckObject stuckObject = stuckObjects[i];
				bool isHittingRigid = false;
				Collider[] hits = Physics.OverlapBox(collider.bounds.center, trs.InverseTransformDirection(collider.bounds.extents) + Vector3.one * Physics.defaultContactOffset, trs.rotation);
				for (int i2 = 0; i2 < hits.Length; i2 ++)
				{
					Collider hit = hits[i2];
					if (hit.GetComponentInParent<Rigidbody2D>() == stuckObject.rigid)
					{
						isHittingRigid = true;
						break;
					}
				}
				if (!isHittingRigid)
				{
					stuckObject.rigid.gravityScale = 1;
					stuckObjects.RemoveAt(i);
					stuckObject.trs.SetParent(stuckObject.previousParent);
					i --;
				}
				else
				{
					stuckObject.trs.SetParent(trs);
					stuckObject.rigid.velocity = Vector2.zero;
					stuckObject.rigid.angularVelocity = 0;
				}
			}
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public struct StuckObject
		{
			public Rigidbody2D rigid;
			public Transform trs;
			public Transform previousParent;

			public StuckObject (Rigidbody2D rigid, Transform trs)
			{
				this.rigid = rigid;
				this.trs = trs;
				previousParent = trs.parent;
			}
		}
	}
}