using UnityEngine;

namespace BulletHell
{
	public class Spawner : MonoBehaviour
	{
		public Transform trs;

		public void DestroyMe (float delay)
		{
			Destroy(gameObject, delay);
		}

		public void FacePlayer ()
		{
			trs.up = Player.instance.trs.position - trs.position;
		}
	}
}