using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace BulletHell
{
	[ExecuteInEditMode]
	public class Enemy : UpdateWhileEnabled, IDestructable, ISpawnable
	{
		int hp;
		public int Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHP;
		public int MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
				maxHP = value;
			}
		}
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public Transform trs;
		public Rigidbody2D rigid;
		public Collider2D collider;
		public Patrol patrol;
		public float moveSpeed;
		public BulletPatternEntry bulletPatternEntry;
		public Timer shootTimer;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public List<UnityEvent> eventsAfterDamaged = new List<UnityEvent>();
		public delegate void OnDeath ();
		public event OnDeath onDeath;

// 		public override void OnEnable ()
// 		{
// #if UNITY_EDITOR
// 			if (!Application.isPlaying)
// 			{
// 				if (trs == null)
// 					trs = GetComponent<Transform>();
// 				if (rigid == null)
// 					rigid = GetComponent<Rigidbody2D>();
// 				if (collider == null)
// 					collider = GetComponent<Collider2D>();
// 				return;
// 			}
// #endif
// 			shootTimer.onFinished += (object[] args) => { bulletPatternEntry.Shoot (); };
// 			shootTimer.Start ();
// 			base.OnEnable ();
// 		}

		public virtual void WakeUp ()
		{
			patrol.enabled = false;
			enabled = true;
		}

		public override void DoUpdate ()
		{
			shootTimer.timeRemaining -= Time.deltaTime;
			if (shootTimer.timeRemaining <= 0)
			{
				bulletPatternEntry.Shoot ();
				shootTimer.timeRemaining += shootTimer.duration;
			}
			HandleMovement ();
		}

		public virtual void HandleMovement ()
		{
		}

		public void TakeDamage (int amount)
		{
			hp -= amount;
			if (hp <= 0)
				Death ();
		}
		
		public void Death ()
		{
			if (onDeath != null)
				onDeath ();
			Destroy(gameObject);
		}

		public void ShootBulletPatternEntry (int index)
		{
			bulletPatternEntries[index].Shoot ();
		}

		// public override void OnDisable ()
		// {
		// 	base.OnDisable ();
		// 	shootTimer.onFinished -= (object[] args) => { bulletPatternEntry.Shoot (); };
		// }
	}
}