using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace BulletHell
{
	public class AcceleratorTile : Tile, ICollisionEnterHandler2D, IUpdatable
	{
		public Collider2D collider;
		public Collider2D Collider
		{
			get
			{
				return collider;
			}
		}
		List<Rigidbody2D> touchingRigids = new List<Rigidbody2D>();
		public float changeMaterialOffsetRate;
		public Material material;
		public float forceAmount;

		public void OnCollisionEnter2D (Collision2D coll)
		{
			Rigidbody2D rigid = coll.gameObject.GetComponentInParent<Rigidbody2D>();
			if (touchingRigids.Contains(rigid))
				return;
			touchingRigids.Add(rigid);
		}

		void OnCollisionStay2D (Collision2D coll)
		{
			OnCollisionEnter2D (coll);
		}

		public override void OnEnable ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (material != null)
				material.mainTextureOffset += Vector2.down * changeMaterialOffsetRate * Time.deltaTime;
			for (int i = 0; i < touchingRigids.Count; i ++)
			{
				Rigidbody2D touchingRigid = touchingRigids[i];
				bool isHittingRigid = false;
				Collider2D[] hits = Physics2D.OverlapBoxAll(collider.bounds.center, collider.bounds.extents + Vector3.one * Physics2D.defaultContactOffset, 0);
				for (int i2 = 0; i2 < hits.Length; i2 ++)
				{
					Collider2D hit = hits[i2];
					if (hit.GetComponentInParent<Rigidbody2D>() == touchingRigid)
					{
						isHittingRigid = true;
						break;
					}
				}
				if (isHittingRigid)
				{
					touchingRigid.AddForce(trs.forward * forceAmount * Time.deltaTime, ForceMode2D.Impulse);
				}
				else
				{
					touchingRigids.RemoveAt(i);
					i --;
				}
			}
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (material != null)
				material.mainTextureOffset = Vector2.zero;
		}
	}
}