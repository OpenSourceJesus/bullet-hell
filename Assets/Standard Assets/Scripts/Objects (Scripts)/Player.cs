using UnityEngine;
using Extensions;
using System;
using System.Collections.Generic;
using UnityEngine.InputSystem;

namespace BulletHell
{
	public class Player : SingletonUpdateWhileEnabled<Player>, IDestructable, ISpawnable
	{
		int hp;
		public int Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHP;
		public int MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
				maxHP = value;
			}
		}
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public Transform trs;
		public Rigidbody2D rigid;
		public Collider2D collider;
		public SyncTransform syncTrs;
		public ushort ID
		{
			get
			{
				return (ushort) syncTrs.id;
			}
			set
			{
				syncTrs.id = value;
			}
		}
		public LayerMask whatICanAttack;
		public LayerMask whatMyDartsHit;
		public Dart dartPrefab;
		public float attackRange;
		public Transform attackRangeVisualizerTrs;
		public Transform attackRangeVisualizerMaskTrs;
		public float attackRangeVisualizerWidth;
		bool leftClickInput;
		bool previousLeftClickInput;

		public override void Awake ()
		{
			base.Awake ();
			hp = maxHP;
			attackRangeVisualizerTrs.SetWorldScale (Vector3.one * attackRange * 2);
			attackRangeVisualizerMaskTrs.SetWorldScale (Vector3.one * (attackRange - attackRangeVisualizerWidth) * 2);
		}

		public override void DoUpdate ()
		{
			leftClickInput = InputManager.LeftClickInput;
			if (InputManager.MoveInput != Vector2.zero)
				trs.up = InputManager.MoveInput;
			HandleMovement ();
			HandleAttacking ();
			previousLeftClickInput = leftClickInput;
		}

		void HandleMovement ()
		{
			Move (InputManager.MoveInput);
		}

		void Move (Vector2 move)
		{
			move = move.Multiply(GameCamera.pixelSize);
			List<RaycastHit2D> hits = new List<RaycastHit2D>();
			if (rigid.Cast(move, hits, move.magnitude) == 0)
				trs.position += (Vector3) move;
			else
				trs.position += (Vector3) move.normalized * hits[0].distance;
		}

		void HandleAttacking ()
		{
			if (InputManager._InputDevice == InputManager.InputDevice.KeyboardAndMouse)
			{
				if (leftClickInput && !previousLeftClickInput)
				{
					Vector2 dartDestination = GameCamera.instance.camera.ScreenToWorldPoint(Mouse.current.position.ReadValue());
					Vector2 toDartDestination = dartDestination - (Vector2) trs.position;
					toDartDestination = Vector2.ClampMagnitude(toDartDestination, attackRange);
					dartDestination = (Vector2) trs.position + toDartDestination;
					Collider2D hitCollider = Physics2D.OverlapPoint(dartDestination, whatICanAttack);
					if (hitCollider != null)
					{
						RaycastHit2D hit = Physics2DExtensions.LinecastWithWidth(trs.position, dartDestination, dartPrefab.size.x, whatMyDartsHit);
						Dart dart = ObjectPool.instance.SpawnComponent<Dart> (dartPrefab.prefabIndex, hit.point, Quaternion.LookRotation(Vector3.forward, toDartDestination));
						Enemy enemy = hit.collider.GetComponent<Enemy>();
						if (enemy != null)
						{
							dart.trs.SetParent(enemy.trs);
							Attack (enemy);
						}
						else
							ObjectPool.instance.DelayDespawn (dart.prefabIndex, dart.gameObject, dart.trs, dart.lifeDuration);
					}
				}
			}
			else
			{
			}
		}

		void Attack (Enemy enemy)
		{
			enemy.TakeDamage (1);
			enemy.eventsAfterDamaged[0].Invoke();
			enemy.eventsAfterDamaged.RemoveAt(0);
		}
		
		public void TakeDamage (int amount)
		{
			hp -= amount;
			if (hp <= 0)
				Death ();
		}
		
		public void Death ()
		{
			_SceneManager.instance.RestartSceneWithoutTransition ();
		}
	}
}