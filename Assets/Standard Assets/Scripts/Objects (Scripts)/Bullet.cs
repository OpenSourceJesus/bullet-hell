﻿using UnityEngine;
using Extensions;
using DelayedDespawn = BulletHell.ObjectPool.DelayedDespawn;

namespace BulletHell
{
	//[ExecuteInEditMode]
	public class Bullet : Spawnable
	{
		public Rigidbody2D rigid;
		public Collider2D collider;
		public float moveSpeed;
		public float lifetime;
		DelayedDespawn delayedDespawn;
		public SpriteRenderer spriteRenderer;
		public Color canRetargetColor;
		public Color cantRetargetColor;
		public bool despawnOnHitDestructable;
		public bool despawnOnHitNonDestructable;
		[HideInInspector]
		public int timesRetargeted;
		[HideInInspector]
		public bool hasFinishedRetargeting;
		public int damageAmountOnHitDestructable = 1;
		public Collider2D shooterCollider;

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				return;
			}
#endif
			rigid.velocity = trs.up * moveSpeed;
			if (lifetime > 0)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, lifetime);
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			IDestructable destructable = other.GetComponent<IDestructable>();
			if (destructable != null)
			{
				destructable.TakeDamage (damageAmountOnHitDestructable);
				if (despawnOnHitDestructable)
				{
					Despawn ();
					return;
				}
			}
			if (despawnOnHitNonDestructable)
				Despawn ();
		}

		public virtual void Despawn ()
		{
			ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		}

		public virtual void Retarget (Vector2 direction, bool lastRetarget = false)
		{
			if (lastRetarget && hasFinishedRetargeting)
				return;
			trs.up = direction;
			rigid.velocity = trs.up * moveSpeed;
			timesRetargeted ++;
			if (lastRetarget)
			{
				hasFinishedRetargeting = true;
				spriteRenderer.color = cantRetargetColor;
				spriteRenderer.sortingOrder --;
			}
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			timesRetargeted = 0;
			if (hasFinishedRetargeting)
			{
				hasFinishedRetargeting = false;
				spriteRenderer.color = canRetargetColor;
				spriteRenderer.sortingOrder ++;
			}
			StopAllCoroutines();
			if (shooterCollider != null)
				Physics2D.IgnoreCollision(collider, shooterCollider, false);
		}
	}
}
