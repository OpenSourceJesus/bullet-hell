﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace BulletHell
{
	public class Patrol : UpdateWhileEnabled
	{
		public float patrolRange;
		public float stopRange;
		public Transform trs;
		public Rigidbody2D rigid;
		public float moveSpeed;
		public LayerMask whatICollideWith;
		Vector2 move;
		Vector2 initPosition;
		Vector2 destination;
		float stopRangeSqr;
		Vector2 toDestination;

		void Awake ()
		{
			initPosition = trs.position;
			stopRangeSqr = stopRange * stopRange;
		}

		public override void OnEnable ()
		{
			SetDestination ();
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			toDestination = destination - (Vector2) trs.position;
			move = Vector2.ClampMagnitude(toDestination, 1);
			move *= moveSpeed;
			trs.up = move;
			rigid.velocity = move;
			if (toDestination.sqrMagnitude <= stopRangeSqr)
				SetDestination ();
		}

		void SetDestination ()
		{
			List<RaycastHit2D> hits = new List<RaycastHit2D>();
			ContactFilter2D contactFilter = new ContactFilter2D();
			contactFilter.useLayerMask = true;
			contactFilter.layerMask = whatICollideWith;
			do
			{
				if (hits.Count > 0 && hits[0].distance == 0)
					return;
				destination = initPosition + (Random.insideUnitCircle * patrolRange);
				toDestination = destination - (Vector2) trs.position;
			} while (rigid.Cast(toDestination, contactFilter, hits, toDestination.magnitude) > 0);
		}
	}
}