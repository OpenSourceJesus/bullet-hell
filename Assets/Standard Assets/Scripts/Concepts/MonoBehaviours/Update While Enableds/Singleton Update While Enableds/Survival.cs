using UnityEngine;
using System;
using Extensions;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using TMPro;

namespace BulletHell
{
	public class Survival : SingletonMonoBehaviour<Survival>
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public int currentDifficulty;
		public int difficultyPerWave;
		public int currentWave;
		public TMP_Text currentWaveText;
		public TMP_Text highestWaveReachedText;
		public int HighestWaveReached
		{
			get
			{
				return PlayerPrefs.GetInt(name + " best wave", 0);
			}
			set
			{
				PlayerPrefs.SetInt(name + " best wave", value);
			}
		}
		int enemiesRemaining;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			ObjectPool.instance = ObjectPool.Instance;
			Player.instance = Player.Instance;
			highestWaveReachedText.text = "Best wave: " + HighestWaveReached;
			NextWave ();
		}

		void NextWave ()
		{
			currentWave ++;
			currentWaveText.text = "Wave: " + currentWave;
			if (currentWave > HighestWaveReached)
			{
				HighestWaveReached = currentWave;
				highestWaveReachedText.text = "Best wave: " + currentWave;
			}
			currentDifficulty += difficultyPerWave;
			float remainingDifficulty = currentDifficulty;
			List<EnemySpawnEntry> remainingEnemySpawnEntries = new List<EnemySpawnEntry>(enemySpawnEntries);
			while (remainingDifficulty > 0)
			{
				int enemySpawnEntryIndex = Random.Range(0, remainingEnemySpawnEntries.Count);
				EnemySpawnEntry enemySpawnEntry = remainingEnemySpawnEntries[enemySpawnEntryIndex];
				if (enemySpawnEntry.difficulty <= remainingDifficulty)
				{
					remainingDifficulty -= enemySpawnEntry.difficulty;
					SpawnEnemy (enemySpawnEntry);
				}
				else
				{
					remainingEnemySpawnEntries.RemoveAt(enemySpawnEntryIndex);
					if (remainingEnemySpawnEntries.Count == 0)
						return;
				}
			}
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector2 spawnPosition;
			do
			{
				CircleCollider2D spawnCircleCollider = enemySpawnEntry.spawnCircleColliders[Random.Range(0, enemySpawnEntry.spawnCircleColliders.Length)];
				spawnPosition = (Vector2) spawnCircleCollider.bounds.center + Random.insideUnitCircle * spawnCircleCollider.bounds.extents.x;
			} while (((Vector2) Player.instance.trs.position - spawnPosition).sqrMagnitude < enemySpawnEntry.minSpawnDistanceToPlayer * enemySpawnEntry.minSpawnDistanceToPlayer);
			Enemy enemy = ObjectPool.instance.SpawnComponent<Enemy>(enemySpawnEntry.enemyPrefab, spawnPosition);
			enemy.onDeath += OnDeath;
			// enemy.trs.forward = (Snake.instance.TailPosition - enemy.eyesTrs.position).GetXZ();
			// enemy.WakeUp ();
			enemiesRemaining ++;
			return enemy;
		}
		
		void OnDeath ()
		{
			enemiesRemaining --;
			if (enemiesRemaining == 0)
				NextWave ();
		}

		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public float difficulty;
			public CircleCollider2D[] spawnCircleColliders;
			public float minSpawnDistanceToPlayer;
		}
	}
}