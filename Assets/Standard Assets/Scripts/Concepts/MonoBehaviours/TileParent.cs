using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BulletHell
{
	public class TileParent : MonoBehaviour
	{
		public Transform trs;
		public Rigidbody2D rigid;
		public ICollisionEnterHandler2D[] collisionEnterHandlers = new ICollisionEnterHandler2D[0];
		// public ICollisionExitHandler2D[] collisionExitHandlers = new ICollisionExitHandler2D[0];
		public static int lastUniqueId = 0;

		void OnCollisionEnter2D (Collision2D coll)
		{
			OnCollisionStay2D (coll);
		}

		void OnCollisionStay2D (Collision2D coll)
		{
			List<ICollisionEnterHandler2D> _collisionEnterHandlers = new List<ICollisionEnterHandler2D>(collisionEnterHandlers);
			for (int i = 0; i < coll.contactCount; i ++)
			{
				ContactPoint2D contactPoint = coll.GetContact(i);
				for (int i2 = 0; i2 < _collisionEnterHandlers.Count; i2 ++)
				{
					ICollisionEnterHandler2D collisionEnterHandler = _collisionEnterHandlers[i2];
					if (contactPoint.collider == collisionEnterHandler.Collider)
					{
						collisionEnterHandler.OnCollisionEnter2D (coll);
						_collisionEnterHandlers.RemoveAt(i2);
						if (_collisionEnterHandlers.Count == 0)
							return;
						break;
					}
				}
			}
		}

		// void OnCollisionExit2D (Collision2D coll)
		// {
		// 	List<ICollisionExitHandler2D> _collisionExitHandlers = new List<ICollisionExitHandler2D>(collisionExitHandlers);
		// 	for (int i = 0; i < coll.contactCount; i ++)
		// 	{
		// 		ContactPoint2D contactPoint = coll.GetContact(i);
		// 		for (int i2 = 0; i2 < _collisionExitHandlers.Count; i2 ++)
		// 		{
		// 			ICollisionExitHandler2D collisionExitHandler = _collisionExitHandlers[i2];
		// 			if (contactPoint.thisCollider == collisionExitHandler.Collider)
		// 			{
		// 				collisionExitHandler.OnCollisionExit2D (coll);
		// 				_collisionExitHandlers.RemoveAt(i2);
		// 				if (_collisionExitHandlers.Count == 0)
		// 					return;
		// 				break;
		// 			}
		// 		}
		// 	}
		// }

		// public void UpdateRigidbody ()
		// {
		// 	rigid.ResetCenterOfMass();
		// 	rigid.ResetInertiaTensor();
		// 	rigid.SetDensity(1);
		// }
	}
}