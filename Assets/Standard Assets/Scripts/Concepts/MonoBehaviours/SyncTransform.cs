using UnityEngine;
using Extensions;
using DarkRift;
using DarkRift.Client;
using DarkRift.Client.Unity;
using DarkRift.Server;
using TMPro;
using System.Collections.Generic;
using System.IO;
using System;
using MessageTags = BulletHell.NetworkManager.MessageTags;

namespace BulletHell
{
	public class SyncTransform : UpdateWhileEnabled
	{
		public Transform trs;
		public uint id;
		public float minDistanceToSyncPosition;
		float minDistanceToSyncPositionSqr;
		Vector3 lastSyncedPosition;

		public override void OnEnable ()
		{
			base.OnEnable ();
			minDistanceToSyncPositionSqr = minDistanceToSyncPosition * minDistanceToSyncPosition;
			// NetworkManager.syncTransformsDict.Add(id, this);
		}
		
		public override void DoUpdate ()
		{
			if ((trs.position - lastSyncedPosition).sqrMagnitude >= minDistanceToSyncPositionSqr)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(id);
					writer.Write(trs.position.x);
					writer.Write(trs.position.y);
					if (NetworkManager.instance.client.enabled)
					{
						using (Message message = Message.Create(MessageTags.SYNC_TRANSFORM_MOVED, writer))
							NetworkManager.instance.client.SendMessage(message, SendMode.Unreliable);
					}
					else
					{
						IClient[] clients = BulletHellPlugin.instance.ClientManager.GetAllClients();
						for (int i = 0; i < clients.Length; i ++)
						{
							IClient client = clients[i];
							using (Message message = Message.Create(MessageTags.SYNC_TRANSFORM_MOVED, writer))
								client.SendMessage(message, SendMode.Unreliable);
						}
					}
				}
				lastSyncedPosition = trs.position;
			}
		}

		// public override void OnDisable ()
		// {
		// 	base.OnDisable ();
		// 	NetworkManager.syncTransformsDict.Remove(id);
		// }
	}
}