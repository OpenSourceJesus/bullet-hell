using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;

[Serializable]
public class FloatRange : Range<float>
{
	public FloatRange (float min, float max) : base (min, max)
	{
	}

	public bool DoesIntersect (FloatRange floatRange, bool equalFloatsIntersect = true)
	{
		if (equalFloatsIntersect)
			return (min >= floatRange.min && min <= floatRange.max) || (floatRange.min >= min && floatRange.min <= max) || (max <= floatRange.max && max >= floatRange.min) || (floatRange.max <= max && floatRange.max >= min);
		else
			return (min > floatRange.min && min < floatRange.max) || (floatRange.min > min && floatRange.min < max) || (max < floatRange.max && max > floatRange.min) || (floatRange.max < max && floatRange.max > min);
	}

	public bool Contains (float f, bool equalFloatsIntersect = true)
	{
		if (equalFloatsIntersect)
			return min <= f && f <= max;
		else
			return min < f && f < max;
	}

	public bool GetIntersectionRange (FloatRange floatRange, out FloatRange intersectionRange, bool equalFloatsIntersect = true)
	{
		intersectionRange = new FloatRange(float.NaN, float.NaN);
		if (DoesIntersect(floatRange, equalFloatsIntersect))
			intersectionRange = new FloatRange(Mathf.Max(min, floatRange.min), Mathf.Min(max, floatRange.max));
		return intersectionRange != new FloatRange(float.NaN, float.NaN);
	}

	public override float Get (float normalizedValue)
	{
		return (max - min) * normalizedValue + min;
	}
}