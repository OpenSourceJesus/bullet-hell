using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace BulletHell
{
	[CreateAssetMenu]
	public class AimWhereFacingWithOffset : AimWhereFacing
	{
		public float shootOffset;
		
		public override Vector2 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(shootOffset);
		}
	}
}