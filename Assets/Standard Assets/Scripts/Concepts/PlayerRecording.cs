using UnityEngine;
using System.Collections.Generic;

public struct PlayerRecording
{
	public List<Frame> frames;

	public struct Frame
	{
		public Vector2 position;
		public Vector2 velocity;
		public float timeSinceCreated;

		public Frame (Vector2 position, Vector2 velocity, float timeSinceCreated)
		{
			this.position = position;
			this.velocity = velocity;
			this.timeSinceCreated = timeSinceCreated;
		}
	}
}