using System;
using UnityEngine;
using System.Collections.Generic;

public class SerializableDictionary<T1, T2> : Dictionary<T1, T2>
{
	public List<T1> keys = new List<T1>();
	public List<T2> values = new List<T2>();
	
	public override void OnDeserialization (object sender = null)
	{
		base.OnDeserialization (sender);
		T1[] _keys = keys.ToArray();
		T2[] _values = values.ToArray();
		keys.Clear();
		values.Clear();
		for (int i = 0; i < _keys.Length; i ++)
			Add (_keys[i], _values[i]);
	}

	public new void Add (T1 key, T2 value)
	{
		base.Add(key, value);
		keys.Add(key);
		values.Add(value);
	}

	public new bool Remove (T1 key)
	{
		T2 value;
		if (TryGetValue(key, out value) && base.Remove(key))
		{
			keys.Remove(key);
			values.Remove(value);
			return true;
		}
		else
			return false;
	}

	public new void Clear (T1 key)
	{
		base.Clear();
		keys.Clear();
		values.Clear();
	}
}