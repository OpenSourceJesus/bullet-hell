using System.Collections;
using System.Collections.Generic;
using DarkRift.Server;
using DarkRift;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using Extensions;
using Random = UnityEngine.Random;
using MessageTags = BulletHell.NetworkManager.MessageTags;

namespace BulletHell
{
	public class BulletHellPlugin : Plugin
	{
		public override Version Version
		{
			get
			{
				return new Version(1, 0, 0);
			}
		}
		public override bool ThreadSafe
		{
			get
			{
				return true;
			}
		}
		public const ushort MAX_PLAYER_COUNT = ushort.MaxValue;
		public static List<ushort> notSpawnedPlayerIds = new List<ushort>();
		public static DateTime endTime = new DateTime(2029, 7, 3, 21, 0, 0, DateTimeKind.Utc);
		public static Dictionary<uint, SyncTransform> npcSyncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static BulletHellPlugin instance;

		public BulletHellPlugin (PluginLoadData pluginLoadData) : base (pluginLoadData)
		{
			instance = this;
			SyncTransform[] npcSyncTransforms = GameManager.FindObjectsOfType<SyncTransform>();
			for (int i = 0; i < npcSyncTransforms.Length; i ++)
			{
				SyncTransform npcSyncTrs = npcSyncTransforms[i];
				npcSyncTransformsDict.Add(npcSyncTrs.id, npcSyncTrs);
				NetworkManager.syncTransformsDict.Add(npcSyncTrs.id, npcSyncTrs);
			}
			ClientManager.ClientConnected += OnClientConnected;
			ClientManager.ClientDisconnected += OnClientDisconnected;
			GameManager.Log ("Irina says hello");
			Task.Factory.StartNew(Update);
		}

		void Update ()
		{
			DateTime startTime = DateTime.UtcNow;
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start ();
			while (true)
			{
				if (startTime + stopwatch.Elapsed >= endTime)
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						client.Disconnect();
					}
				}
			}
		}

		void OnClientConnected (object sender, ClientConnectedEventArgs eventArgs)
		{
			eventArgs.Client.MessageReceived += OnPreSpawnMessageReceived;
		}

		void OnClientDisconnected (object sender, ClientDisconnectedEventArgs eventArgs)
		{
			ushort playerId = eventArgs.Client.ID;
			if (NetworkManager.playersDict.ContainsKey(playerId))
			{
				int indexOfNotSpawnedPlayerId = notSpawnedPlayerIds.IndexOf(playerId);
				if (indexOfNotSpawnedPlayerId > -1)
				{
					notSpawnedPlayerIds.RemoveAt(indexOfNotSpawnedPlayerId);
					return;
				}
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(playerId);
					using (Message message = Message.Create(MessageTags.PLAYER_LEFT, writer))
					{
						IClient[] clients = ClientManager.GetAllClients();
						for (int i = 0; i < clients.Length; i ++)
						{
							IClient client = clients[i];
							if (client != eventArgs.Client)
								client.SendMessage(message, SendMode.Reliable);
						}
					}
				}
				Player player = NetworkManager.playersDict[playerId];
				ObjectPool.instance.Despawn (player.prefabIndex, player.gameObject, player.trs);
				NetworkManager.playersDict.Remove(playerId);
				NetworkManager.syncTransformsDict.Remove(playerId);
				eventArgs.Client.MessageReceived -= OnPostSpawnMessageReceived;
			}
			else
				eventArgs.Client.MessageReceived -= OnPreSpawnMessageReceived;
		}

		void OnPreSpawnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				if (message.Tag == MessageTags.SPAWN_PLAYER)
					OnSpawnPlayer (sender, eventArgs);
			}
		}

		void OnSpawnPlayer (object sender, MessageReceivedEventArgs eventArgs)
		{
			if (NetworkManager.playersDict.Count >= MAX_PLAYER_COUNT)
			{
				notSpawnedPlayerIds.Add(eventArgs.Client.ID);
				eventArgs.Client.Disconnect();
				return;
			}
			byte spawnPointIndex;
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
					spawnPointIndex = reader.ReadByte();
			}
			NetworkManager.SpawnPoint spawnPoint = NetworkManager.instance.spawnPoints[spawnPointIndex];
			Player player = ObjectPool.instance.SpawnComponent<Player>(NetworkManager.instance.playerPrefab.prefabIndex, spawnPoint.trs.position, spawnPoint.trs.rotation);
			player.ID = eventArgs.Client.ID;
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(player.ID);
				writer.Write(spawnPoint.trs.position.x);
				writer.Write(spawnPoint.trs.position.y);
				writer.Write(spawnPoint.trs.position.z);
				writer.Write(spawnPoint.trs.eulerAngles.y);
				using (Message message = Message.Create(MessageTags.SPAWN_PLAYER, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				foreach (Player previousPlayer in NetworkManager.playersDict.Values)
				{
					writer.Write(previousPlayer.ID);
					writer.Write(previousPlayer.trs.position.x);
					writer.Write(previousPlayer.trs.position.y);
					writer.Write(previousPlayer.trs.position.z);
					writer.Write(previousPlayer.trs.eulerAngles.y);
					using (Message message = Message.Create(MessageTags.SPAWN_PLAYER, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			foreach (SyncTransform npcSyncTrs in npcSyncTransformsDict.Values)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(npcSyncTrs.id);
					writer.Write(npcSyncTrs.trs.position.x);
					writer.Write(npcSyncTrs.trs.position.y);
					writer.Write(npcSyncTrs.trs.position.z);
					using (Message message = Message.Create(MessageTags.SYNC_TRANSFORM_MOVED, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(npcSyncTrs.id);
					writer.Write(npcSyncTrs.trs.eulerAngles.y);
					using (Message message = Message.Create(MessageTags.SYNC_TRANSFORM_ROTATED, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			NetworkManager.playersDict.Add(player.ID, player);
			NetworkManager.syncTransformsDict.Add(player.ID, player.syncTrs);
			eventArgs.Client.MessageReceived -= OnPreSpawnMessageReceived;
			eventArgs.Client.MessageReceived += OnPostSpawnMessageReceived;
		}

		void OnPostSpawnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				if (message.Tag == MessageTags.SPAWN_PLAYER)
					OnSpawnPlayer (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_MOVED)
					OnSyncTransformMoved (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_ROTATED)
					OnSyncTransformRotated (sender, eventArgs);
				// else if (message.Tag == MessageTags.PLAYER_STARTED_ACTION)
				// 	OnPlayerStartedAction (sender, eventArgs);
			}
		}

		void OnSyncTransformMoved (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					SyncTransform syncTrs;
					if (NetworkManager.syncTransformsDict.TryGetValue(reader.ReadUInt32(), out syncTrs))
					{
						syncTrs.trs.position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						IClient[] clients = ClientManager.GetAllClients();
						for (int i = 0; i < clients.Length; i ++)
						{
							IClient client = clients[i];
							if (client != eventArgs.Client)
								client.SendMessage(message, eventArgs.SendMode);
						}
					}
				}
			}
		}

		void OnSyncTransformRotated (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					SyncTransform syncTrs;
					if (NetworkManager.syncTransformsDict.TryGetValue(reader.ReadUInt32(), out syncTrs))
					{
						syncTrs.trs.eulerAngles = Vector3.up * reader.ReadSingle();
						IClient[] clients = ClientManager.GetAllClients();
						for (int i = 0; i < clients.Length; i ++)
						{
							IClient client = clients[i];
							if (client != eventArgs.Client)
								client.SendMessage(message, eventArgs.SendMode);
						}
					}
				}
			}
		}

		// void OnPlayerStartedAction (object sender, MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage() as Message)
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			using (DarkRiftWriter writer = DarkRiftWriter.Create())
		// 			{
		// 				writer.Write(eventArgs.Client.ID);
		// 				writer.Write(reader.ReadByte());
		// 				writer.Write(reader.ReadDouble());
		// 				using (Message newMessage = Message.Create(MessageTags.PLAYER_STARTED_ACTION, writer))
		// 				{
		// 					IClient[] clients = ClientManager.GetAllClients();
		// 					for (int i = 0; i < clients.Length; i ++)
		// 					{
		// 						IClient client = clients[i];
		// 						if (client != eventArgs.Client)
		// 							client.SendMessage(newMessage, SendMode.Reliable);
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }
	}
}